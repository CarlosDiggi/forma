import unittest
import forma

class TestForma(unittest.TestCase):
    def test_url_conexion(self):
        """Debe formar la url de conexion"""
        # aqui falla
        url = forma.rest({'host':'diggi.duckdns.org','puerto':2480},'connect', 'prueba')
        expected = 'http://diggi.duckdns.org:2480/connect/prueba'
        self.assertEqual(url, expected)

    def test_url_consulta(self):
        """Debe formar la url de conexion"""
        # aqui falla
        url = forma.rest({'host':'diggi.duckdns.org','puerto':2480},'command', 'prueba/sql/-')
        # deberia permitir llamadas como
        # url = forma.rest({'host':'diggi.duckdns.org','puerto':2480},'command', 'prueba', 'sql', '-')
        expected = 'http://diggi.duckdns.org:2480/command/prueba/sql/-'
        self.assertEqual(url, expected)

if __name__ == '__main__':
    unittest.main()